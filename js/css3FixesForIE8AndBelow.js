$(document).ready(function(){

	function css3Fixes(){
		// support fallback for CSS3
		$('nav').css('height', '100%').css('height', '-=70px');
		$('#wrapper').css('height', '100%').css('height', '-=70px');
		$('#mainColumn').css('height', '100%').css('height', '-=20px');
		$('#mainColumn').css('width', '100%').css('width', '-=170px');
		$('article').css('height', '40%').css('height', '-=10px');
		$('main').css('height', '40%').css('height', '-=10px');
	}
	css3Fixes();

	$( window ).resize(function() {
		css3Fixes();
	});
});

// provides support for below css
// nav {height:calc(100% - 70px)}
// #wrapper {height:calc(100% - 70px)}
// #mainColumn {height:calc(100% - 20px); width:calc(100% - 170px)}
// article, main {height:calc(40% - 10px)}